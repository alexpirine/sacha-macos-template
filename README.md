# Things to install on a new Mac

- [Chrome](https://www.google.com/intl/fr_fr/chrome/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [brew](https://brew.sh/)
- [Sequel Ace](https://sequel-ace.com/)
- [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
- [Tailscale](https://tailscale.com/download/)
- [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- [Obsidian](https://obsidian.md/)
- [WhatsApp](https://www.whatsapp.com/download)
- [Signal](https://signal.org/fr/download/)
- [VLC](https://www.videolan.org/vlc/index.fr.html)

## Installation script

```bash
# installs brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# activates brew for zsh
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> "$HOME/.zprofile"
eval "$(/opt/homebrew/bin/brew shellenv)"

# opts-out from brew analytics
brew analytics off

# installs gls (from coreutils) and bash
brew install coreutils bash

# installs nano and enables syntax highlighting
brew install nano
echo '# enables syntax highlighting' >>  ~/.nanorc
echo 'include /opt/homebrew/share/nano/*.nanorc' >> ~/.nanorc

# marks the brew-installed bash as a valid login shell
echo "$(brew --prefix)/bin/bash" | sudo tee -a /etc/shells

# changes default shell to bash for the current user
chsh -s "$(brew --prefix)/bin/bash"

# activates brew for bash
echo '# activates brew' >> "$HOME/.bash_profile"
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> "$HOME/.bash_profile"

# launches bash
$(brew --prefix)/bin/bash

# installs bash completions for brew-installed software
brew install bash-completion

# installs Git
brew install git
mkdir -p ~/.git/hooks

# installs Python
brew install python@3.12
brew install python@3.11
brew install python@3.10

# installs PDM package manager for Python
curl -sSL https://raw.githubusercontent.com/pdm-project/pdm/main/install-pdm.py | python3 -

# installs virtualenvwrapper for Python
brew install virtualenvwrapper

# installs useful dependencies for various libraries
brew install pkg-config mariadb-connector-c mysql-client

# installs NodeJS and updates it to the latest available version
brew install node
npm install -g npm

# disables Next.JS telemetry data colleciton
echo >> ~/.bash_profile
echo "# Disables Next.JS telemetry data colleciton" >> ~/.bash_profile
echo "NEXT_TELEMETRY_DISABLED=1"  >> ~/.bash_profile

# installs OpenJDK to run Java apps
brew install java
echo >> ~/.bash_profile
echo '# Uses java binary provided by homebrew' >> ~/.bash_profile
echo 'export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"' >> ~/.bash_profile

# installs gcloud CLI
mkdir -p ~/opt && cd ~/opt
curl -o gcloud-cli.tar.gz 'https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-darwin-arm.tar.gz'
tar -xzf gcloud-cli.tar.gz
./google-cloud-sdk/install.sh

# installs Cloud SQL Auth Proxy
cd google-cloud-sdk/bin
curl -o cloud-sql-proxy https://storage.googleapis.com/cloud-sql-connectors/cloud-sql-proxy/v2.11.4/cloud-sql-proxy.darwin.arm64
chmod +x cloud-sql-proxy
```
