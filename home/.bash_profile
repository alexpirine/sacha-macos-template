# activates brew
eval "$(/opt/homebrew/bin/brew shellenv)"

# activates bash completions for brew-installed software
[[ -r "$(brew --prefix)/etc/profile.d/bash_completion.sh" ]] && . "$(brew --prefix)/etc/profile.d/bash_completion.sh"

# reads bash aliases
if [ -r ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

# Sets up the prompt look&feel
PS1='\[\e[1;33m\][\A] \[\e[1;32m\]🐳\[\e[0m\]:\[\e[1;34m\]\w\[\e[0m\]\$\[\e[0m\] '
PROMPT_DIRTRIM=2

# Adds various Python versions to PATH
export PATH="$(brew --prefix)/opt/python@3.10/libexec/bin:$PATH"
export PATH="$(brew --prefix)/opt/python@3.11/libexec/bin:$PATH"

# makes Python 3.12 the default
export PATH="$(brew --prefix)/opt/python@3.12/libexec/bin:$PATH"
export PATH="$HOME/Library/Python/3.12/bin:$PATH"

# The next lin sets Python encoding to UTF-8 by default
export PYTHONIOENCODING=utf-8

# activates virtualenvwrapper
source "$(brew --prefix)/bin/virtualenvwrapper_lazy.sh"

# Enables the Tailscale CLI
alias tailscale="/Applications/Tailscale.app/Contents/MacOS/Tailscale"
