alias ll="gls -Faoh --color=auto --time-style=long-iso --group-directories-first"

alias genpwd="
    LC_ALL=C \
    tr -dc [:print:] < /dev/urandom \
      | tr -d '\`^' \
      | fold -w 20 \
      | head -n 1 ;
    LC_ALL=C \
    tr -dc [:alnum:] < /dev/urandom \
      | fold -w 20 \
      | head -n 1
"
